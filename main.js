let cols = 10;
let rows = 20;
let score = 0;

let grid = [];
let canvas = document.getElementById("game");
let context = canvas.getContext("2d");

/**
 * Create the grid, made from cells.
 * For each cell, we use canvas to draw it on the DOM.
 */
const drawGrid = () => {
    for (let row = 0; row < rows; row++) {
        grid[row] = [];
        for (let column = 0; column < cols; column++) {
            grid[row][column] = new Cell(row, column);
            grid[row][column].draw();
        }
    }
}

/**
 * Method used to redraw the grid when we must score.
 */
const redrawGrid = () => {
    for (let row = 0; row < rows; row++) {
        for (let column = 0; column < cols; column++) {
            let cell = grid[row][column];
            cell.x = row * 30;
            cell.y = column * 30;
            cell.draw(cell.color);
        }
    }
}

drawGrid();
document.getElementById('score').innerText = score;