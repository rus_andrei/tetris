class I extends Shape {
    squareColor = '#f0f8ff';
    constructor(x) {
        const template = [
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0]
        ];
        super(template, 0, 4);
    }
}

class J extends Shape {
    squareColor = 'blue';
    constructor(x, y) {
        const template = [
            [0, 1, 0],
            [0, 1, 0],
            [1, 1, 0],
        ];
        super(template, 0, 4);
    }
}

class L extends Shape {
    squareColor = 'orange';
    constructor(y) {
        const template = [
            [1, 0, 0],
            [1, 0, 0],
            [1, 1, 0]
        ]
        super(template, 0, 4);
    }
}

class S extends Shape {
    squareColor = 'green';
    constructor() {
        const template = [
            [0, 0, 0],
            [0, 1, 1],
            [1, 1, 0]
        ];

        super(template, 0, 4);
    }
}

class O extends Shape {
    squareColor = 'yellow';
    constructor() {
        const template = [
            [1, 1],
            [1, 1]
        ];
        super(template, 0, 4);
    }
}

class T extends Shape {
    squareColor = 'magenta';
    constructor() {
        const template = [
            [1, 0, 0],
            [1, 1, 0],
            [1, 0, 0]
        ];
        super(template, 0, 4);
    }
}

class Z extends Shape {
    squareColor = 'red';
    constructor() {
        const template = [
            [0, 0, 0],
            [1, 1, 0],
            [0, 1, 1]
        ];

        super(template, 0, 4);
    }
}