class Shape {
    clearColor = "#888888";
    canNotMove = false;
    constructor(template, x, y) {
        this.template = template;
        this.level = 0;
        this.x = x;
        this.y = y;
    }

    /**
     * Rotate the current template to the right by ninety-degree.
     */
    rotate() {
        this.draw(true);
        let n = this.template.length;
        for (var i = 0; i < n / 2; i++) {
            for (var j = i; j < n - i - 1; j++) {
                var tmp = this.template[i][j];
                this.template[i][j] = this.template[j][n - i - 1];
                this.template[j][n - i - 1] = this.template[n - i - 1][n - j - 1];
                this.template[n - i - 1][n - j - 1] = this.template[n - j - 1][i];
                this.template[n - j - 1][i] = tmp;
            }
        }
        this.draw();
        return this.template;
    }

    /**
     * This method will draw the shape on the grid.
     * @param {boolean} clear    - If clear flag is true then use the clearColor, 
     *                           - otherwise use the shape color
     *                           - it can be hexa, rgb or simple("blue").   
     */
    draw(clear) {
        let color = clear ? this.clearColor : this.squareColor;
        for (let i = 0; i < this.template.length; i++) {
            for (let j = 0; j < this.template[i].length; j++) {
                if (this.template[i][j]) {
                    if (grid[this.x + i][this.y + j]) {
                        grid[this.x + i][this.y + j].draw(color);
                        grid[this.x + i][this.y + j].color = color;
                    }
                    grid[this.x + i][this.y + j].isEmpty = clear || false;
                }

            }
        }
    }

    checkNext(x, y) {
        for (let i = 0; i < this.template.length; i++) {
            for (let j = 0; j < this.template[i].length; j++) {
                let gridCell = grid[x + i] && grid[x + i][y + j];
                let isEmpty = !!gridCell && gridCell.isEmpty;
                if (this.template[i][j] && !isEmpty) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if the next move is inside grid or not.
     * x, y can be increased/decreased with 1 unit based on movment direction.
     *  
     * @param {number} x    - X coordonate.
     * @param {number} y    - Y coordonate.
     * Returns "true" if next position is out of the grid, "false" otherwise.
     */
    isOutsideTheGrid(x, y) {
        for (let i = 0; i < this.template.length; i++) {
            for (let j = 0; j < this.template[i].length; j++) {
                let insideGrid = grid[x + i] && grid[x + i][y + j];
                if (this.template[i][j] && !insideGrid) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Move down the current shape, if the next position is available.
     * clear the old shape, increase x and repaint the shape.
     * other wise stop the animation and exit the method.
     */
    move() {
        let notInGrid = this.isOutsideTheGrid(this.x + 1, this.y);

        if (notInGrid) {
            clearInterval(this.intervalId);
            this.canNotMove = true;
            return;
        }

        this.draw(true);

        let nextNotAvailable = this.checkNext(this.x + 1, this.y);

        if (nextNotAvailable) {
            this.draw();
            clearInterval(this.intervalId);
            this.canNotMove = true;
            return;
        }

        this.level++;
        this.x++;
        this.draw();
    }

    /**
     * Move to left the current shape.
     * Check if next position is available, if "true"
     * clear the old shape, decrease y and repaint the shape
     * other wise exit the method.
     */
    moveLeft() {
        let notInGrid = this.isOutsideTheGrid(this.x, this.y - 1);
        if (notInGrid) {
            return;
        }
        this.draw(true);

        let nextNotAvailable = this.checkNext(this.x, this.y - 1);

        if (nextNotAvailable) {
            this.draw();
            return;
        }

        this.y--;
        this.draw();
    }

    /**
     * Move to right the current shape.
     * Check if next position is available, if "true"
     * clear the old shape, increase y and repaint the shape
     * other wise exit the method.
     */
    moveRight() {
        let notInGrid = this.isOutsideTheGrid(this.x, this.y + 1);

        if (notInGrid) {
            return;
        }

        this.draw(true);

        let nextNotAvailable = this.checkNext(this.x, this.y + 1);

        if (nextNotAvailable) {
            this.draw();
            return;
        }

        this.y++;
        this.draw();
    }

    /**
     * Start animation "fall down" for the current shape.
     */
    animate() {
        this.intervalId = setInterval(() => {
            this.move();
        }, 200);
    }
}