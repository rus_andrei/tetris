let currentShape, shapes = [
    { instance: () => new I() },
    { instance: () => new J() },
    { instance: () => new L() },
    { instance: () => new O() },
    { instance: () => new S() },
    { instance: () => new T() },
    { instance: () => new Z() }
];

/**
 * Generate random numbers between 0 and max.
 * @param {number} max   - The upper limit for numbers.
 */
const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Generate a random shape based on the index.
 */
const generateNewShape = () => {
    const index = getRandomInt(shapes.length);
    return shapes[index].instance();
}

/**
 * Generate a new row which will be attached at the beggining of the grid.
 */
const generateNewRow = () => {
    let newRow = [];
    for (let i = 0; i < cols; i++) {
        newRow.push(new Cell());
    }

    return newRow;
}

/**
 * Start the game, generate the shape and make it move.
 */
const startGame = () => {
    currentShape = generateNewShape();
    currentShape.animate();

    // Simulate an infinite loop.
    let gameInterval = setInterval(() => {
        if (currentShape && currentShape.canNotMove) {
            if (currentShape.level === 0) {
                clearInterval(gameInterval);
                alert('Game Over');
                return;
            }
            currentShape = generateNewShape();
            console.log('Level: ' + currentShape.level);
            currentShape.animate();
            let lines = fullLines();

            // if we have any full lines, delete those
            // reattach the same numbers of rows at the begging of the grid.
            // redraw the grid;
            if (lines.length) {
                lines.forEach(rowIndex => deleteLine(rowIndex));
                lines.forEach(() => attachGridRows());
                score += lines.length * 10;
                document.getElementById('score').innerText = score;
                redrawGrid();
            }
        }
    }, 500);
}

/**
 * Event that is executed each time a key is pressed.
 * @param {KeydownEvent} event
 */
const handleKeyDown = (event) => {
    switch (event.key) {
        case 'ArrowLeft':
            currentShape.moveLeft();
            break;
        case 'ArrowRight':
            currentShape.moveRight();
            break;
        case 'ArrowUp':
            currentShape.rotate();
            break;
    }
}

/**
 * Get the rows which are fully completed.
 */
const fullLines = () => {
    let fullRows = [];
    for (let row = 0; row < rows; row++) {
        if (isFullLine(row)) {
            fullRows.push(row);
        }

    }
    return fullRows;
}

/**
 * Delete the given row formn the grid.
 * @param {number} line - Represents the line number.
 */
const deleteLine = (line) => {
    grid.splice(line, 1);
}

/**
 * Attach rows at the begging of the grid.
 */
const attachGridRows = () => {
    let newArray = generateNewRow();
    grid.unshift(newArray);
}

/**
 * Check if a specific line is fully ocupied by shapes.
 * @param {number} line - The line number which is checked. 
 */
const isFullLine = (line) => {
    return grid[line].every(item => !item.isEmpty);
}

/**
 * Add keydown event listener for the document.
 */
document.addEventListener('keyup', handleKeyDown.bind(this));