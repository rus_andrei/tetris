class Cell {
    constructor(row, column, color) {
        this.x = row * 30;
        this.y = column * 30;
        this.isEmpty = true;
        this.color = color || "#888888";
    }

    draw(color) {
        context.beginPath();
        context.fillStyle = color || this.color;
        context.lineWidth = 2;
        context.strokeStyle = "black";
        context.rect(this.y, this.x, 30, 30);
        context.fill();
        context.stroke();
        context.closePath();
    }
}