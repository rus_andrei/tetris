# Tetris Game

Tetris game build with JS (ES6 and canvas)

## Done

```bash
- Created grid.
- Created all shapes.
- Rotate, move left, right.
- Fall down animation.
- Validate left, right, down border
- Collisions.
- Check for full lines, score (delete lines), make those available and translate the grid.
- Score.
- Game over logic.
```

## To do

```bash
- Code refines.
```

## In progress

```bash
- Small adjustments on rotate action near borders.
```